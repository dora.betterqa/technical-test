// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

Cypress.Commands.add('openHomePage', () => {
	cy.visit('/')
})

Cypress.Commands.add('search', (movie) => {
	let search = movie
	cy.get('[type="search"]').type(`${search}{enter}`)
	cy.request('GET', `https://api.themoviedb.org/3/search/movie?api_key=eb65eb0fd35b0dffbe34d74febb34589&query=${search}`)
		.then(res => {
			expect(res.status).to.equal(200)
		})
})

Cypress.on('uncaught:exception', (err) => {
	return false
})


