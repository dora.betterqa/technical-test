function checkImg() {
    cy.get('.movie-image').each(($image) => {
        expect($image).to.have.css('background-image').to.include('.jpg')
    })
}


export class Movies {

    checkMovieList() {
        cy.get('.movies').should('be.visible')
        cy.get('.movie-card').should('have.length', '20')
        cy.get('.movie-image').should('have.length', '20')
        checkImg()
        cy.get('.movies').find('h2').should('have.length', '20')
        cy.get('.movies').find('p').should('have.length', '20')
        cy.get('.movies').find('button').should('have.length', '20')
    }

    checkImage() {
        checkImg()
    }

    checkReleaseDate(movie, id) {
        cy.get('.movies').find('.movie-card')
            .should('contain', movie)
            .then(() => cy.get('[type="button"]').eq(id).contains('Learn More').click())
            .then(() => cy.get('[type="text"]').eq(0).should('have.value', '1994-09-23'))
    }

    checkAssert(movie, value) {
        cy.get('.movie-card').contains('h2', movie).should(value)
    }

    checkMovieDetails(movie, id) {
        let date, popularity, voteAverage, voteCount
        cy.request({
            method: 'GET',
            url: 'https://api.themoviedb.org/3/movie/top_rated?api_key=eb65eb0fd35b0dffbe34d74febb34589',
            params: {
                'api_key': 'eb65eb0fd35b0dffbe34d74febb34589'
            }
        }).then(response => {
            expect(response.status).to.equal(200)
            date = response.body.results[3].release_date
            popularity = response.body.results[3].popularity
            voteAverage = response.body.results[3].vote_average
            voteCount = response.body.results[3].vote_count
        })
        cy.get('.movies').find('.movie-card')
            .should('contain', movie)
            .then(() => cy.get('[type="button"]').eq(id).contains('Learn More').click())
            .then(() => cy.get('[type="text"]').eq(0).should('have.value', date))
            .then(() => cy.get('[type="text"]').eq(1).should('have.value', popularity))
            .then(() => cy.get('[type="text"]').eq(2).should('have.value', voteAverage))
            .then(() => cy.get('[type="text"]').eq(3).should('have.value', voteCount))
            .then(() => cy.get('[type="button"]').contains('span', 'Close').should('be.visible').click())

    }

    searchMovie(movie) {
        let search = movie
        cy.get('[type="search"]').type(`${search}{enter}`)
        cy.request('GET', `https://api.themoviedb.org/3/search/movie?api_key=eb65eb0fd35b0dffbe34d74febb34589&query=${search}`)
            .then(res => {
                expect(res.status).to.equal(200)
            })

    }
}
export const movie = new Movies();