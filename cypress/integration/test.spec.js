import { movie } from "../support/Utils/movies"

describe('Testing the website', () => {

    beforeEach('Open application', () => {
        cy.openHomePage()
    })

    it('Check if you are able to see the movie list when you access the app', () => {
        movie.checkMovieList()
    })

    it('Check release date for a given movie', () => {
        movie.checkReleaseDate('The Shawshank Redemption', 2)
    })

    it('Search for a given movie and check what is displayed', () => {
        movie.searchMovie("Star Trek")
        movie.checkAssert('Star Trek: First Contact', 'be.visible')
        movie.checkAssert('The Shawshank Redemption', 'not.exist')
    })

    it('Check the details of a given movie', () => {
        movie.checkMovieDetails('The Godfather', '3')
    })

    it('Search with null value/blank spaces - bug', () => {
        cy.search("")
    })

    it('Search & check movie images - bug', () => {
        movie.searchMovie("xd")
        movie.checkImage()
    })
})